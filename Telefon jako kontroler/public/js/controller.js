﻿document.addEventListener('DOMContentLoaded', function () {
	var statusTag = document.querySelector("#status");
	function noSupportFallback() {		
		statusTag.style.color = "red";
		statusTag.innerText = "Twoje urządzenie lub przeglądarka nie wspiera wymaganej technologii";
	};
	if (!('DeviceOrientationEvent' in window)) {
		noSupportFallback();
		return;
	}
	var socket = io.connect("http://tel.szymek.xyz:3001");
	
	socket.on("message", function (msg) {
		if (msg == "desktopdc") {
			statusTag.style.color = "red";
			statusTag.innerText = "Monitor został odłączony";
		}
	});
	socket.emit("join room", roomID);

	statusTag.innerText = "Urządzenie gotowe!";

	var lastX = 0, lastY = 0, lastZ = 0;
	window.addEventListener("deviceorientation", function (event) {
		let x = event.beta;
		let y = event.gamma;
		let z = event.alpha;
		if (x == null || y == null || z == null) {
			noSupportFallback();
			return;
		}
		if (Math.abs(x - lastX) >= 1 || Math.abs(y - lastY) >= 1  || Math.abs(z - lastZ) >= 1)
			socket.emit("orientation", { x: x, y: y, z: z });
		lastX = x;
		lastY = y;
		lastZ = z;
	});
});