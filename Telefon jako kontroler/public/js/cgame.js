﻿const RECT_WIDTH = 100, RECT_HEIGHT = 200;
var canvas = canvasWidth = canvasHeight = ctx = x = y = z = canvasCenter = null;
window.addEventListener("DOMContentLoaded", function () {
	var socket = io.connect("http://tel.szymek.xyz:3001");
	canvas = document.querySelector("#gl");
	canvasWidth = window.innerWidth * .75;
	canvasHeight = window.innerHeight * .75;
	canvas.width = canvasWidth;
	canvas.height = canvasHeight;
	ctx = canvas.getContext("2d");
	ctx.font = "11px Segoe UI";
	canvasCenter = {
		x: canvasWidth / 2 - RECT_WIDTH,
		y: canvasHeight / 2 - RECT_HEIGHT
	};
	
	socket.on("message", function (msg) {
		if (msg == "mobiledc") {
			$("section").addClass("hidden");
			$("#mobiledc").removeClass("hidden");
		}
		else if (msg == "mobileconnected") {
			$("section").addClass("hidden");
			$("#game").removeClass("hidden");
		}
	});
	socket.on("orientation", function (data) {
		x = data.x;
		y = data.y;
		z = data.z;
		window.requestAnimationFrame(draw);
	});	
	
	socket.emit("join room desktop", roomID);
	draw();
});
function draw() {
	ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	ctx.fillStyle = "white";
	ctx.fillText("X: " + Math.round(x), 10, 10);
	ctx.fillText("Y: " + Math.round(y), 10, 22);
	ctx.fillText("Z: " + Math.round(z), 10, 34);
	ctx.save();

	ctx.beginPath();
	ctx.translate(canvasCenter.x + RECT_WIDTH / 2, canvasCenter.y + RECT_HEIGHT / 2);
	ctx.rotate(z * Math.PI / 180);
	ctx.rect(-RECT_WIDTH / 2, -RECT_HEIGHT / 2, RECT_WIDTH, RECT_HEIGHT);
	ctx.strokeStyle = "blue";
	ctx.stroke();	
	ctx.restore();
};