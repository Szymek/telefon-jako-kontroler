﻿var shortID = require("shortid");

var Room = function (id) {
	this.ID = id;
	this.MobileSocket = null;
	this.DesktopSocket = null;
	var self = this;
	this.SetDesktopSocket = function (socket) {
		this.DesktopSocket = socket;
		socket.on("disconnect", function () {
			delete Game.Rooms.List[this.ID];
			if (self.MobileSocket != null) {
				self.MobileSocket.send("desktopdc");
				self.MobileSocket.disconnect();
			}
			console.log("Socket [*] [t: desktop, isMobileNull: " + (self.MobileSocket == null) + "]");
		});
	};
	this.SetMobileSocket = function (socket) {
		this.MobileSocket = socket;
		if (this.DesktopSocket != null)
			this.DesktopSocket.send("mobileconnected");		

		socket.on("disconnect", function () {
			delete Game.Rooms.List[this.ID];
			if (self.DesktopSocket != null) {
				self.DesktopSocket.send("mobiledc");
				self.DesktopSocket.disconnect();
			}
			console.log("Socket [*] [t: mobile, isDesktopNull: " + (self.DesktopSocket == null) + "]");
		});

		socket.on("orientation", function (data) {
			if (self.DesktopSocket != null)
				self.DesktopSocket.emit("orientation", data);
		});
	};
};

var Game = {};
Game.Rooms = {List: {}};
Game.Rooms.New = function (id) {
	id = id || shortID.generate().toLocaleLowerCase();
	let room = new Room(id);
	Game.Rooms.List[id] = room;
	return room;
};
Game.Rooms.Remove = function (id) {
	delete Game.Rooms.List[id];
};


module.exports = Game;