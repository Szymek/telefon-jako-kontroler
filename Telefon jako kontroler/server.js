﻿var express = require("express");
var app = express();
var io = require("socket.io")();
var Game = require("./game.js");

app.set("view engine", "jade");
app.set("views", __dirname + "/views");
app.enable("view cache");


app.use("/public", express.static(__dirname + "/public"));
app.use("/", require("./routes/index.js"));
app.use("/r", require("./routes/room.js"));

app.listen(3000);


io.on("connection", function (socket) {
	let handler = function (id, desktop) {
		if (id in Game.Rooms.List) {
			if (desktop)
				Game.Rooms.List[id].SetDesktopSocket(socket);
			else
				Game.Rooms.List[id].SetMobileSocket(socket);

			console.log("Socket connected [desktop: " + desktop + "]");
		}
	};
	socket.on("join room desktop", (id) => { handler(id, true) });
	socket.on("join room", (id) => { handler(id, false) });
});
io.listen(3001);