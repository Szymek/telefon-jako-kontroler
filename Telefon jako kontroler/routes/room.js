﻿var router = require("express").Router();
var Game = require("../game.js");

router.get("/:rid", function (req, res) {
	var id = req.params.rid;
	if (!(id in Game.Rooms.List)) {
		res.end("Nieprawidłowy adres");
		return;
	}
	res.render("controller", { roomID: id });
});

module.exports = router;