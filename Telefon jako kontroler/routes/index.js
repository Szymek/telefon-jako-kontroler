﻿var router = require("express").Router();
var Game = require("../game");

router.get("/", function (req, res) {
	let room = Game.Rooms.New();
	res.render("game", { roomID: room.ID });
});

module.exports = router;